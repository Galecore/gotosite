# [GoTo](http://goto.msk.ru)'s take at HRMS

## Deployment

```sh
git clone git@bitbucket.org:Galecore/gotosite.git
cd gotosite
virtualenv -p python2 env
source env/bin/activate
pip install -r requirements.txt
python manage.py runserver
```
