"""gotosite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf import settings

from django.conf.urls import include, url
from django.conf.urls.static import static

from django.contrib import admin
from goto import views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.mainpage, name='mainpage'),
    url(r'^disconnect/$', views.disconnect, name='disconnect'),
    url(r'^actions/sign_in/$', views.sign_in_action, name='sign_in_action'),
    url(r'^actions/sign_up/$', views.sign_up_action, name='sign_up_action'),
    url(r'^actions/sign_out/$', views.sign_out_action, name='sign_out_action'),
    url(r'^profile/$', views.profile, name='profile'),
    url(r'^about_us/$', views.about_us, name='about_us'),
    url(r'^hack/$', views.hackathon, name='hakathon'),
    url(r'^lecture/$', views.lecture, name='lecture'),
    url(r'^camp/$', views.camp, name='camp'),
    url(r'^take_part/$', views.take_part, name='take_part'),
    url(r'^pages/$', views.pages, name='pages'),
    url(r'^actions/apply/$', views.apply, name='apply'),
    url(r'^actions/subscribe/$', views.subscribe, name='subscribe_action'),
    url(r'^actions/edit_profile/$', views.edit_profile_action, name='edit_profile_action'),
    url(r'^actions/apply/$', views.apply, name='apply'),
    url(r'^actions/refuse/$', views.refuse, name='refuse'),
    url(r'^actions/write_comment_action/$', views.write_comment_action, name='write_comment_action'),
    # url(r'', include('social.apps.django_app.urls', namespace='social'))
    # url(r'^send/$', views.send_letter, name='send_letter'),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    # url(r'^events/(?P<url>.+)/$', views.event, name='event'),