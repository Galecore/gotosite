# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import class_prepared

from datetime import date

def longer_username(sender, *args, **kwargs):
    if sender.__name__ == "User" and sender.__module__ == "django.contrib.auth.models":
        sender._meta.get_field("username").max_length = 75

class_prepared.connect(longer_username)

class CampUser(models.Model): 
    user = models.OneToOneField(User)

    first_name = models.CharField(max_length = 140, null=True, blank=True)
    middle_name = models.CharField(max_length = 140, null=True, blank=True)
    last_name = models.CharField(max_length = 140, null=True, blank=True)

    profile_picture = models.ImageField(upload_to='profile_pictures/', null=True, blank=True)

    birthday = models.DateField(default=date.today)

    place_of_birth = models.CharField(max_length = 140, null=True, blank=True)

    city = models.CharField(max_length = 40, default='Москва', blank=True)
    citizenship = models.CharField(max_length = 40, default='Российская Федерация', blank=True)

    phone_number = models.IntegerField(blank=True, null=True)
    parents_phone_number = models.IntegerField(blank=True, null=True)

    occupation = models.CharField(max_length = 250, blank=True, default='', null=True)
    programming_languages = models.CharField(max_length = 250, blank=True, default='', null=True)

    document = models.CharField(max_length = 40, blank=True, default='', null=True)
    document_number = models.IntegerField(blank=True, null=True)
    police_number = models.IntegerField(blank=True, null=True)

    vk = models.URLField(max_length = 240, default='', blank=True)
    github = models.URLField(max_length = 240, default='', blank=True)

    health_issues = models.CharField(max_length = 40, default='Никаких', blank=True)

    experience = models.CharField(max_length = 700, default='', blank=True)
    about = models.CharField(max_length = 500, default='', blank=True)

    def __str__(self):
        return self.user.username
    class Meta:
        verbose_name_plural = 'CampUsers'

class Participant(models.Model):
    user = models.ForeignKey('Campuser')
    event = models.ForeignKey('Event')
    status = models.ForeignKey('ParticipantStatus')
    def __str__(self):
        return self.user.user.username
    class Meta:
        verbose_name_plural = 'Participants'

class Teacher(models.Model):
    name = models.CharField(max_length = 500,)
    description = models.CharField(max_length = 500,)
    teacher_image = models.ImageField(upload_to='teacher_images/', null=True, blank=True)
    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = 'Teachers'

class News(models.Model):
    title = models.CharField(max_length = 500,)
    description = models.CharField(max_length = 500,)
    news_image = models.ImageField(upload_to='news_images/', null=True, blank=True)
    def __str__(self):
        return self.title
    class Meta:
        verbose_name_plural = 'News'

class Event(models.Model):
    event_type = models.ForeignKey('EventTypes')
    name = models.CharField(max_length=140)
    description = models.CharField(max_length = 400)
    event_date = models.DateField(default=date.today)
    format = models.CharField(max_length = 400, blank=True, default='')
    place = models.CharField(max_length = 400, blank=True, default='')
    url = models.CharField(max_length=140, unique=True)
    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = 'Events'

class Question(models.Model):
    question = models.CharField(max_length=300)
    answer = models.CharField(max_length=300, blank=True, default='')
    def __str__(self):
        return self.question
    class Meta:
        verbose_name_plural = 'Questions'

class Project(models.Model):
    name = models.CharField(max_length=300)
    authors = models.ManyToManyField('CampUser')
    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = 'Projects'

class EventTypes(models.Model):
    event_type = models.CharField(max_length = 140)
    def __str__(self):
        return self.event_type

class ParticipantStatus(models.Model):
    status = models.CharField(max_length = 140)
    def __str__(self):
        return self.status


class Partner(models.Model):
    name = models.CharField(max_length=160)
    url = models.CharField(max_length=160)
    image = models.ImageField(upload_to='news_pictures/', null=True, blank=True)
    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = 'Partners'  

class Subscriber(models.Model):
    email = models.EmailField()

    def __str__(self):
        return self.email
    class Meta:
        verbose_name_plural = 'Subscribers'  