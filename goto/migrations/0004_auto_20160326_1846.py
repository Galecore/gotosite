# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0003_auto_20160319_1755'),
    ]

    operations = [
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default='', max_length=1240, blank=True)),
            ],
            options={
                'verbose_name_plural': 'CampUsers',
            },
        ),
        migrations.CreateModel(
            name='School',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default='', max_length=160, blank=True)),
                ('grade', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'verbose_name_plural': 'School',
            },
        ),
        migrations.AddField(
            model_name='campuser',
            name='parents_phone_number',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='campuser',
            name='phone_number',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='campuser',
            name='school',
            field=models.ForeignKey(to='goto.School', null=True),
        ),
    ]
