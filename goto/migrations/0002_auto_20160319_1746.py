# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='campuser',
            name='about',
            field=models.CharField(default='', max_length=160, blank=True),
        ),
        migrations.AddField(
            model_name='campuser',
            name='city',
            field=models.CharField(default='Москва', max_length=40, blank=True),
        ),
        migrations.AddField(
            model_name='campuser',
            name='day',
            field=models.IntegerField(default=1, blank=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='campuser',
            name='month',
            field=models.IntegerField(default=1, blank=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='campuser',
            name='year',
            field=models.IntegerField(default=1999, blank=True),
            preserve_default=False,
        ),
    ]
