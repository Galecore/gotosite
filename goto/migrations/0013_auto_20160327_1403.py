# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0012_auto_20160327_1333'),
    ]

    operations = [
        migrations.CreateModel(
            name='Participant',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'verbose_name_plural': 'Participants',
            },
        ),
        migrations.CreateModel(
            name='ParticipantStatus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.CharField(max_length=140)),
            ],
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length='140')),
                ('authors', models.ManyToManyField(to='goto.CampUser')),
            ],
            options={
                'verbose_name_plural': 'Projects',
            },
        ),
        migrations.AlterModelOptions(
            name='event',
            options={'verbose_name_plural': 'Events'},
        ),
        migrations.AddField(
            model_name='event',
            name='event_type',
            field=models.ForeignKey(default=2, to='goto.EventTypes'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='event',
            name='name',
            field=models.CharField(max_length=140, blank=True),
        ),
        migrations.AddField(
            model_name='participant',
            name='status',
            field=models.ForeignKey(to='goto.ParticipantStatus'),
        ),
        migrations.AddField(
            model_name='participant',
            name='user',
            field=models.ForeignKey(to='goto.CampUser'),
        ),
        migrations.AddField(
            model_name='event',
            name='participants',
            field=models.ManyToManyField(to='goto.Participant'),
        ),
    ]
