# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0024_auto_20160328_2231'),
    ]

    operations = [
        migrations.AddField(
            model_name='campuser',
            name='username',
            field=models.CharField(default=1, max_length=140),
            preserve_default=False,
        ),
    ]
