# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0027_auto_20160329_0221'),
    ]

    operations = [
        migrations.AlterField(
            model_name='campuser',
            name='first_name',
            field=models.CharField(max_length=140, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='campuser',
            name='last_name',
            field=models.CharField(max_length=140, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='campuser',
            name='middle_name',
            field=models.CharField(max_length=140, null=True, blank=True),
        ),
    ]
