# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0011_auto_20160327_1240'),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='EventTypes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('event_type', models.CharField(max_length=140)),
            ],
        ),
        migrations.RemoveField(
            model_name='project',
            name='authors',
        ),
        migrations.RemoveField(
            model_name='campuser',
            name='projects',
        ),
        migrations.DeleteModel(
            name='Project',
        ),
    ]
