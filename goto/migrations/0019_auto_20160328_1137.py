# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0018_auto_20160327_1749'),
    ]

    operations = [
        migrations.RenameField(
            model_name='campuser',
            old_name='day',
            new_name='birthdate_day',
        ),
        migrations.RenameField(
            model_name='campuser',
            old_name='month',
            new_name='birthdate_month',
        ),
        migrations.RenameField(
            model_name='campuser',
            old_name='year',
            new_name='birthdate_year',
        ),
    ]
