# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0014_auto_20160327_1536'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name='participants',
        ),
    ]
