# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0051_auto_20160509_1126'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name='questions',
        ),
    ]
