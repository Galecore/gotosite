# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0037_campuser_health_issues'),
    ]

    operations = [
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('comment_data', models.CharField(max_length=400)),
                ('user', models.ForeignKey(to='goto.CampUser')),
            ],
            options={
                'verbose_name_plural': 'Commentaries',
            },
        ),
    ]
