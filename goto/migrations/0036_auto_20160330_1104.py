# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0035_auto_20160329_1326'),
    ]

    operations = [
        migrations.AddField(
            model_name='campuser',
            name='document',
            field=models.CharField(default='', max_length=40, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='campuser',
            name='document_number',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='campuser',
            name='place_of_birth',
            field=models.CharField(max_length=140, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='campuser',
            name='police_number',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
