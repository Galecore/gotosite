# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0054_auto_20160509_1215'),
    ]

    operations = [
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=500)),
                ('description', models.CharField(max_length=500)),
                ('news_image', models.ImageField(null=True, upload_to='news_images/', blank=True)),
            ],
            options={
                'verbose_name_plural': 'News',
            },
        ),
    ]
