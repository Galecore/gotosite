# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0013_auto_20160327_1403'),
    ]

    operations = [
        migrations.AlterField(
            model_name='participant',
            name='status',
            field=models.NullBooleanField(),
        ),
        migrations.DeleteModel(
            name='ParticipantStatus',
        ),
    ]
