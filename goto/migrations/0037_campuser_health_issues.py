# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0036_auto_20160330_1104'),
    ]

    operations = [
        migrations.AddField(
            model_name='campuser',
            name='health_issues',
            field=models.CharField(default='Никаких', max_length=40, blank=True),
        ),
    ]
