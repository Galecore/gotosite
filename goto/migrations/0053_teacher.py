# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0052_remove_event_questions'),
    ]

    operations = [
        migrations.CreateModel(
            name='Teacher',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=500)),
                ('description', models.CharField(max_length=500)),
                ('teacher_images', models.ImageField(null=True, upload_to='teacher_images/', blank=True)),
            ],
            options={
                'verbose_name_plural': 'Teachers',
            },
        ),
    ]
