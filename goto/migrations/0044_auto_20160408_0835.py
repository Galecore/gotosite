# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0043_delete_plaintextpage'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='day',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='event',
            name='month',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
    ]
