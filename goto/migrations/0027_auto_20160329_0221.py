# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0026_remove_campuser_username'),
    ]

    operations = [
        migrations.AddField(
            model_name='campuser',
            name='first_name',
            field=models.CharField(default=1, max_length=140),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='campuser',
            name='last_name',
            field=models.CharField(default=1, max_length=140),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='campuser',
            name='middle_name',
            field=models.CharField(default=1, max_length=140),
            preserve_default=False,
        ),
    ]
