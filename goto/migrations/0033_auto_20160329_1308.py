# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0032_auto_20160329_1100'),
    ]

    operations = [
        migrations.CreateModel(
            name='Answer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('answer_data', models.CharField(max_length=400)),
                ('answerer', models.ForeignKey(to='goto.CampUser')),
            ],
            options={
                'verbose_name_plural': 'Answers',
            },
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question_data', models.CharField(max_length=400)),
                ('answers', models.ManyToManyField(to='goto.Answer')),
            ],
            options={
                'verbose_name_plural': 'Questions',
            },
        ),
        migrations.AddField(
            model_name='event',
            name='questions',
            field=models.ManyToManyField(to='goto.Question'),
        ),
    ]
