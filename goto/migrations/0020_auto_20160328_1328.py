# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0019_auto_20160328_1137'),
    ]

    operations = [
        migrations.CreateModel(
            name='ParticipantStatus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.CharField(max_length=140)),
            ],
        ),
        migrations.AlterField(
            model_name='participant',
            name='status',
            field=models.ForeignKey(default=1, to='goto.ParticipantStatus'),
            preserve_default=False,
        ),
    ]
