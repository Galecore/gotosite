# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0005_auto_20160326_1903'),
    ]

    operations = [
        migrations.AddField(
            model_name='campuser',
            name='citizenship',
            field=models.CharField(default='Российская Федерация', max_length=40, blank=True),
        ),
        migrations.AddField(
            model_name='campuser',
            name='projects',
            field=models.ManyToManyField(to='goto.Project', null=True),
        ),
    ]
