# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0046_auto_20160408_2331'),
    ]

    operations = [
        migrations.AddField(
            model_name='campuser',
            name='experience',
            field=models.CharField(default='', max_length=700, blank=True),
        ),
    ]
