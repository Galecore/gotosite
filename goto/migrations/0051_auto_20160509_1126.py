# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0050_auto_20160509_1045'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='format',
            field=models.CharField(default='', max_length=400, blank=True),
        ),
        migrations.AddField(
            model_name='event',
            name='place',
            field=models.CharField(default='', max_length=400, blank=True),
        ),
    ]
