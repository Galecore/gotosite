# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0008_auto_20160327_1023'),
    ]

    operations = [
        migrations.AlterField(
            model_name='campuser',
            name='projects',
            field=models.ManyToManyField(to='goto.Project', blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='authors',
            field=models.ManyToManyField(to='goto.CampUser', blank=True),
        ),
    ]
