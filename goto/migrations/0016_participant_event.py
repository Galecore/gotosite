# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0015_remove_event_participants'),
    ]

    operations = [
        migrations.AddField(
            model_name='participant',
            name='event',
            field=models.ForeignKey(default=1, to='goto.Event'),
            preserve_default=False,
        ),
    ]
