# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0053_teacher'),
    ]

    operations = [
        migrations.RenameField(
            model_name='teacher',
            old_name='teacher_images',
            new_name='teacher_image',
        ),
    ]
