# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0049_auto_20160506_0005'),
    ]

    operations = [
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question', models.CharField(max_length=300)),
                ('answer', models.CharField(default='', max_length=300, blank=True)),
            ],
            options={
                'verbose_name_plural': 'Questions',
            },
        ),
        migrations.AddField(
            model_name='event',
            name='questions',
            field=models.ManyToManyField(to='goto.Question'),
        ),
    ]
