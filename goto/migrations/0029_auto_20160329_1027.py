# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0028_auto_20160329_0228'),
    ]

    operations = [
        migrations.CreateModel(
            name='Answer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('reply', models.CharField(max_length=1000)),
                ('answerer', models.ForeignKey(to='goto.CampUser')),
            ],
            options={
                'verbose_name_plural': 'Answers',
            },
        ),
        migrations.AddField(
            model_name='question',
            name='answers',
            field=models.ManyToManyField(to='goto.Answer'),
        ),
    ]
