# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0017_auto_20160327_1643'),
    ]

    operations = [
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question', models.CharField(max_length=300)),
            ],
            options={
                'verbose_name_plural': 'Questions',
            },
        ),
        migrations.AlterField(
            model_name='project',
            name='name',
            field=models.CharField(max_length=300),
        ),
        migrations.AddField(
            model_name='event',
            name='questions',
            field=models.ManyToManyField(to='goto.Question', blank=True),
        ),
    ]
