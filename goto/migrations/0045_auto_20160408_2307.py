# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0044_auto_20160408_0835'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='campuser',
            name='birthdate_day',
        ),
        migrations.RemoveField(
            model_name='campuser',
            name='birthdate_month',
        ),
        migrations.RemoveField(
            model_name='campuser',
            name='birthdate_year',
        ),
        migrations.AddField(
            model_name='campuser',
            name='birthday',
            field=models.DateField(default=datetime.date.today),
        ),
    ]
