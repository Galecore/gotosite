# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0007_project_authors'),
    ]

    operations = [
        migrations.DeleteModel(
            name='School',
        ),
        migrations.DeleteModel(
            name='SocialNetworks',
        ),
    ]
