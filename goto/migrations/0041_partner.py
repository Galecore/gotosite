# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0040_news_plaintextpage'),
    ]

    operations = [
        migrations.CreateModel(
            name='Partner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=160)),
                ('url', models.CharField(max_length=160)),
                ('image', models.ImageField(null=True, upload_to='news_pictures/', blank=True)),
            ],
            options={
                'verbose_name_plural': 'Partners',
            },
        ),
    ]
