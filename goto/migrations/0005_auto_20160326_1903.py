# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0004_auto_20160326_1846'),
    ]

    operations = [
        migrations.CreateModel(
            name='SocialNetworks',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'verbose_name_plural': 'SocialNetworks',
            },
        ),
        migrations.RemoveField(
            model_name='campuser',
            name='school',
        ),
        migrations.RemoveField(
            model_name='school',
            name='grade',
        ),
        migrations.RemoveField(
            model_name='school',
            name='name',
        ),
        migrations.AddField(
            model_name='campuser',
            name='github',
            field=models.URLField(default='', max_length=240, blank=True),
        ),
        migrations.AddField(
            model_name='campuser',
            name='image',
            field=models.ImageField(null=True, upload_to='/media/profile_pictures/'),
        ),
        migrations.AddField(
            model_name='campuser',
            name='place_of_work',
            field=models.CharField(default='', max_length=160, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='campuser',
            name='school_grade',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='campuser',
            name='school_name',
            field=models.CharField(default='', max_length=160, blank=True),
        ),
        migrations.AddField(
            model_name='campuser',
            name='vk',
            field=models.URLField(default='', max_length=240, blank=True),
        ),
    ]
