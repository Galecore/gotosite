# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0002_auto_20160319_1746'),
    ]

    operations = [
        migrations.AlterField(
            model_name='campuser',
            name='day',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='campuser',
            name='month',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='campuser',
            name='year',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
