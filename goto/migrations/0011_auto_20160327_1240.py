# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0010_auto_20160327_1032'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='campuser',
            name='image',
        ),
        migrations.AddField(
            model_name='campuser',
            name='profile_picture',
            field=models.ImageField(null=True, upload_to='profile_pictures/', blank=True),
        ),
    ]
