# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0045_auto_20160408_2307'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='campuser',
            name='place_of_work',
        ),
        migrations.RemoveField(
            model_name='campuser',
            name='school_grade',
        ),
        migrations.RemoveField(
            model_name='campuser',
            name='school_name',
        ),
        migrations.AddField(
            model_name='campuser',
            name='occupation',
            field=models.CharField(default='', max_length=250, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='campuser',
            name='programming_languages',
            field=models.CharField(default='', max_length=250, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='campuser',
            name='about',
            field=models.CharField(default='', max_length=500, blank=True),
        ),
    ]
