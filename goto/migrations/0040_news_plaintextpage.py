# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0039_event_description'),
    ]

    operations = [
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data', models.CharField(max_length=5000)),
                ('url', models.CharField(unique=True, max_length=160)),
                ('image', models.ImageField(null=True, upload_to='news_pictures/', blank=True)),
            ],
            options={
                'verbose_name_plural': 'News',
            },
        ),
        migrations.CreateModel(
            name='PlainTextPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('page_data', models.CharField(max_length=5000)),
                ('url', models.CharField(unique=True, max_length=160)),
            ],
            options={
                'verbose_name_plural': 'Plain Text Pages',
            },
        ),
    ]
