# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0022_auto_20160328_2223'),
    ]

    operations = [
        migrations.AlterField(
            model_name='campuser',
            name='profile_picture',
            field=models.ImageField(height_field=250, width_field=250, null=True, upload_to='profile_pictures/', blank=True),
        ),
    ]
