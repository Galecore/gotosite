# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0033_auto_20160329_1308'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='questions',
            field=models.ManyToManyField(to='goto.Question', blank=True),
        ),
        migrations.AlterField(
            model_name='question',
            name='answers',
            field=models.ManyToManyField(to='goto.Answer', blank=True),
        ),
    ]
