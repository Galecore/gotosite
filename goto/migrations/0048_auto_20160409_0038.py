# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('goto', '0047_campuser_experience'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name='day',
        ),
        migrations.RemoveField(
            model_name='event',
            name='month',
        ),
        migrations.AddField(
            model_name='event',
            name='event_date',
            field=models.DateField(default=datetime.date.today),
        ),
    ]
