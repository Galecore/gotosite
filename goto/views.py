# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.http import HttpResponse 
from django.template import RequestContext, loader

from django.contrib.auth.models import User

from django.contrib.auth import login, logout, authenticate
from django.http import HttpResponseRedirect
from django.template.context_processors import csrf
from goto.models import CampUser, Event, Participant, ParticipantStatus, EventTypes, Subscriber, Question, Teacher, News
from hashlib import md5
from django.core.mail import send_mail
from datetime import date
import smtplib

from django.contrib.auth.decorators import login_required





# Авторизация - backend
def sign_in_action(request):
    email = request.POST.get('email')
    password = request.POST.get('password')
    # print(email)
    # print(password)
    try:
        
        user = authenticate(username=email, password=password)
        if user and user.is_active:
            login(request, user)
    except:
        return HttpResponseRedirect('/disconnect')
    return HttpResponseRedirect("/")

def sign_up_action(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    email = request.POST.get('email')
    print ((username, password, email))
    try:
        user = User.objects.create_user(username=email, email=email)
        user.set_password(password)
        user.save()
        user_log = authenticate(username=email, password=password)
        login(request, user_log)
        
    except: 
        return HttpResponseRedirect("/disconnect")
    camp_user = CampUser.objects.create(user=user)
    camp_user.first_name, camp_user.last_name = username.split()
    camp_user.save()
    return HttpResponseRedirect("/")

def sign_out_action(request):
    logout(request)
    return HttpResponseRedirect('/') 

# def sign_in_through_vk(request):
#     username = request.GET.get('first_name') + '' + request.GET.get('last_name')
#     vkhash = request.GET.get('hash')
#     try:
#         user = authenticate(username=username, password=vkhash)
#         if user and user.is_active:
#                 login(request, user)
#     except:
#         user = User.objects.create_user(username)
#         user.set_password(password)
#         user.save()
#     return HttpResponseRedirect('/')
# Главная страница


def mainpage(request):
    template = loader.get_template('goto/index.html')
    if request.user.is_active:
        context_dictionary = {'campuser':CampUser.objects.get(user=request.user), 'news':News.objects.all()}
    else:
        context_dictionary = {}
    context_dictionary.update(csrf(request))
    context = RequestContext(request, context_dictionary)
    return HttpResponse(template.render(context))

# Комментарии

def write_comment_action(request):
    comment_data = request.POST.get('comment_data')
    user = User.objects.get(username=request.user.username)
    campuser = CampUser.objects.get(user=user)
    comment = Comment(user=campuser, comment_data=comment_data)
    comment.save()
    return HttpResponseRedirect('/')



def vklogin(request):
    template = loader.get_template('goto/vklogin.html')
    context = RequestContext(request, {})
    return HttpResponse(template.render(context))


# Аккаунт

@login_required(login_url='/')
def profile(request):
    template = loader.get_template('goto/profile.html')
    user = User.objects.get(username=request.user.username)
    campuser = CampUser.objects.get(user=user)
    context_dictionary = {'campuser':campuser, 'participants':Participant.objects.filter(user=campuser), 'events':Event.objects.filter(event_date__gte=date.today())[:4]}
    context_dictionary.update(csrf(request))
    context = RequestContext(request, context_dictionary)
    return HttpResponse(template.render(context))

# О нас
def about_us(request):
    template = loader.get_template('goto/about_us.html')
    if request.user.is_active:
        context_dictionary = {'campuser':CampUser.objects.get(user=request.user), 'teachers':Teacher.objects.all()}
    else:
        context_dictionary = {}
    context = RequestContext(request, context_dictionary)
    return HttpResponse(template.render(context))

def hackathon(request):
    template = loader.get_template('goto/hackathon.html')
    if request.user.is_active:
        context_dictionary = {'campuser':CampUser.objects.get(user=request.user)}
    else:
        context_dictionary = {}
    context = RequestContext(request, context_dictionary)
    return HttpResponse(template.render(context))

def pages(request):
    template = loader.get_template('goto/pages.html')
    context = RequestContext(request, {})
    return HttpResponse(template.render(context))

def take_part(request):
    template = loader.get_template('goto/take_part.html')
    context = RequestContext(request, {})
    return HttpResponse(template.render(context))
    
def lecture(request):
    template = loader.get_template('goto/lecture.html')
    if request.user.is_active:
        context_dictionary = {'campuser':CampUser.objects.get(user=request.user), 'first_lectures':Event.objects.filter(event_type=EventTypes.objects.get(event_type='lecture')).order_by('-event_date',)[:2], 'other_lectures':Event.objects.filter(event_type=EventTypes.objects.get(event_type='lecture')).order_by('-event_date',)[2:]}
    else:
        context_dictionary = {}
    context = RequestContext(request, context_dictionary)
    return HttpResponse(template.render(context))

def camp(request):
    template = loader.get_template('goto/camp.html')
    if request.user.is_active:
        context_dictionary = {'campuser':CampUser.objects.get(user=request.user), 'first_camps':Event.objects.filter(event_type=EventTypes.objects.get(event_type='camp')).order_by('-event_date',)[:3], 'other_camps':Event.objects.filter(event_type=EventTypes.objects.get(event_type='camp')).order_by('-event_date',)[3:], 'questions':[question for question in Question.objects.all()], 'teachers':Teacher.objects.all()}
    else:
        context_dictionary = {}
    context = RequestContext(request, context_dictionary)
    print(context_dictionary)
    return HttpResponse(template.render(context))


def edit_profile_action(request):
    user, campuser = identify_user(request)
    profile_info = form_profile_info()
    for field in request.POST:
        # print(field, end=' '); print(request.POST.get(field));
        if field not in profile_info:
            continue
        if request.POST.get(field):
            profile_info[field](campuser, request.POST.get(field))
    return HttpResponseRedirect('/')


def apply(request):
    event_id = request.GET.get('id')
    user = User.objects.get(username=request.user.username)
    campuser = CampUser.objects.get(user=user)
    participant = Participant.objects.filter(event=Event.objects.get(id=event_id), user=campuser)
    if participant:
        participant = participant[0]
        participant.status = ParticipantStatus.objects.get(status='Рассматривается')
        participant.save()
    else:
        participant = Participant.objects.create(user=campuser, event=Event.objects.get(id=event_id), status=ParticipantStatus.objects.get(status='Рассматривается'))
        participant.save()
    return HttpResponseRedirect('/')

def refuse(request):
    event_id = request.GET.get('id')
    user = User.objects.get(username=request.user.username)
    campuser = CampUser.objects.get(user=user)
    refused_participant = Participant.objects.get(user=campuser, event=Event.objects.get(id=event_id))
    refused_participant.status = ParticipantStatus.objects.get(status='Отказался')
    refused_participant.save()
    return HttpResponseRedirect('/')   

# Отсоединение

def disconnect(request):
    template = loader.get_template('goto/disconnect.html')
    context_dictionary = {}
    context = RequestContext(request, context_dictionary)
    return HttpResponse(template.render(context))








def subscribe(request):
    email = request.POST.get('email')
    if email not in Subscriber.objects.all():
        new_subscriber = Subscriber(email=email)
        new_subscriber.save()
    return HttpResponseRedirect('/')
    
# def send_little_mail():
#     server = smtplib.SMTP_SSL('smtp.gmail.com', port=465)
#     server.login('tima.shunin@gmail.com', 'gotocamp_2015')
#     server.sendmail('tima.shunin@gmail.com', 'bibilov@yandex.ru', 'Hello! :)')
#     server.quit()


# def send_letter(request):
#     email_text = 'Hi!'
#     send_little_mail(email_text)
#     return HttpResponseRedirect('/')

def identify_user(request):
    user = User.objects.get(username=request.user.username)
    campuser = CampUser.objects.get(user=user)
    return user, campuser

def form_profile_info():
    profile_info = {
        'birthday':  save_birthday,
        'experience':  save_experience,
        'phone_number':  save_phone_number,      
        'place_of_birth':  save_place_of_birth,
        'vk':  save_vk,
        'programming_languages':  save_programming_languages,
        'github':  save_github,
        'about':  save_about,
        'health_issues':  save_health_issues,    
        'city':  save_city,    
        'occupation':  save_occupation,    
    }
    return profile_info

def save_birthday(campuser, info):
    d,m,y = info.split('.')
    campuser.birthday = date(int(y), int(m), int(d))
    campuser.save()

def save_citizenship(campuser, info):
    campuser.citizenship = info
    campuser.save()

def save_city(campuser, info):
    campuser.city = info
    campuser.save()

def save_phone_number(campuser, info):
    campuser.phone_number = info
    campuser.save()

def save_parents_phone_number(campuser, info):
    campuser.parents_phone_number = info
    campuser.save()

def save_place_of_birth(campuser, info):
    campuser.place_of_birth = info
    campuser.save()

def save_occupation(campuser, info):
    campuser.occupation = info
    campuser.save()

def save_programming_languages(campuser, info):
    campuser.programming_languages = info
    campuser.save()

def save_vk(campuser, info):
    campuser.vk = info
    campuser.save()

def save_github(campuser, info):
    campuser.github = info
    campuser.save()

def save_experience(campuser, info):
    campuser.experience = info
    campuser.save()

def save_about(campuser, info):
    campuser.about = info
    campuser.save()

def save_health_issues(campuser, info):
    campuser.health_issues = info
    campuser.save()
