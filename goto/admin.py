from django.contrib import admin

from goto.models import CampUser, Project, Event, Participant, EventTypes, ParticipantStatus, Subscriber, Question, Teacher, News

class ParticipantInline(admin.TabularInline):
    model = Participant

class EventAdmin(admin.ModelAdmin):
    inlines = [
        ParticipantInline,
    ]
    exclude = ('participants',)

models = [CampUser, Project, Participant,  Subscriber,  Question, Teacher, News]

for model in models:
    admin.site.register(model)
admin.site.register(Event, EventAdmin)