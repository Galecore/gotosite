from django import template

register = template.Library()

@register.filter
def first(value): 
    a = value.split()
    return a[0]

@register.filter
def last(value):
	a = value.split()
	return ' '.join(a[1:])